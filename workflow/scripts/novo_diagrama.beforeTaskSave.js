function beforeTaskSave(colleagueId, nextSequenceId, userList) {

  var atividade = getValue("WKNumState");


  var nAnexos = hAPI.getCardValue("nCerti");
  log.info('Chegou aqui os anexos ' + nAnexos);
  if (atividade == 0 || atividade == 4) {
    var anexos = hAPI.listAttachments();
    if (anexos.size() == 0) {
      throw "É preciso anexar o roteiro para continuar o processo!";
    }
  }
  if (atividade == 2 || atividade == 6) {
    var anexos = hAPI.listAttachments();
    if (anexos.size() - 1 < nAnexos) {
      throw "É preciso anexar os certificados para continuar o processo!";
    }
    if (anexos.size() - 1 > nAnexos) {
      throw "Você anexou mais certificados do que o necessário, remova na aba anexos o anexo excedente";
    }else {
      var calendar = java.util.Calendar.getInstance().getTime();
      var desc_dcto = hAPI.getCardValue("nomeDestino");
      
      //Criar pasta com nome de acordo com o valor de um campo
      var dto = docAPI.newDocumentDto();
      dto.setDocumentDescription(desc_dcto); // nome da pasta que sera criada
      dto.setDocumentType("1");
      dto.setParentDocumentId(64053); // codigo da pasta que se deseja criar as sub-pastas
      dto.setDocumentTypeId("");

      var folder = docAPI.createFolder(dto, null, null);
      log.info("PASTA CRIADA COM O ID:" + folder.getDocumentId());
      

      hAPI.setCardValue("campohidden", folder.getDocumentId()); 

      
      for (var i = 0; i < anexos.size(); i++) {
        var docDto = anexos.get(i);
        //Enviar Anexo para Documentos do fluig
        docDto.setParentDocumentId(folder.getDocumentId());
        docDto.setVersionDescription("Processo: " + getValue("WKNumProces"));
        docDto.setExpires(false);
        docDto.setCreateDate(calendar);
        docDto.setInheritSecurity(true);
        docDto.setTopicId(1);
        docDto.setUserNotify(false);
        docDto.setValidationStartDate(calendar);
        docDto.setVersionOption("0");
        docDto.setUpdateIsoProperties(true);
        hAPI.publishWorkflowAttachment(docDto);
      }
    }
  }
}
