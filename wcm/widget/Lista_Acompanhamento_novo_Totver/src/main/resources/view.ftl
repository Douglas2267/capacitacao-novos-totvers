<div id="MyWidget_${instanceId}" class="super-widget wcm-widget-class fluig-style-guide" data-params="MyWidget.instance({})">
<script src='/webdesk/vcXMLRPC.js'></script>
    <div class="fluig-style-guide2">
        <div class="panel panel-primary">
            <div class="panel-heading"><h2><b>Pesquisa</b></h2></div>
            <div class="panel-body">
                <div class="row">
                    <div class="form-group col-md-4">
                        <label for="nomeGestor">Nome Gestor</label>
                        <select id="nomeGestor" name="nomeGestor" class="form-control">
                            <option value="">Selecione</option>
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label>Nome do responsavel do processo(RH)</label>
                        <input type="text" id="rh" name="rh" class="form-control"/>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-3">
                        <label>Data de inicio do precesso </label>
                        <input type="text" id="dtInicio" name="dtInicio" class="form-control"/>
                    </div>
                    <div class="form-group col-md-3">
                        <label>Data de fim do precesso </label>
                        <input type="text" id="dtFim" name="dtFim" class="form-control"/>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-3">
                        <button class="btn btn-success" data-buscar>Buscar</button>
                    </div>
                </div>
                <div id="div_proc" class="col-md-8" style="display:grid;clear:left;margin-top:15px;">
                <label for="table_proc" style="text-align:center">Processos</label>
                <table class="table" id="table_proc">
                    <thead>
                        <tr>
                            <td id="proc_instanceId">Nº Registro</td>
                            <td id="proc_stat">Status</td>
                            <td id="proc_databe">Data abertura</td>
                            <td id="proc_req">Responsável pelo registro</td>
                            <td id="proc_clas">Classificação</td>
                            <td id="proc_resp">Responsável pela ação</td>
                            <td id="proc_descnc">Descrição da não conformidade</td>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                <table>
            </div>
        </div> 
    <div>
    <script>
        var settings = {
            source: {
                url:  '/api/public/ecm/dataset/search?datasetId=colleague&searchField=colleagueName&',
                contentType: 'application/json',
                root: 'content',
                pattern: '',
                limit: 1,
                offset: 0,
                patternKey: 'searchValue',
                limitkey: 'limit',
                offsetKey: 'offset'
            },
            displayKey: 'colleagueName',
            multiSelect: true,
            style: {
                autocompleteTagClass: 'tag-gray',
                tableSelectedLineClass: 'info'
            },
            table: {
                header: [
                    {
                        'title': 'Nome',
                        'size': 'col-xs-12',
                        'dataorder': 'colleagueName',
                        'standard': true
                    }
                ],
                renderContent: ['colleagueName']
            }
        }
    var filter = FLUIGC.filter('#rh', settings);
    var dtInicio = FLUIGC.calendar('#dtInicio');
     var dtFim = FLUIGC.calendar('#dtFim');
    </script>
</div>

