var MyWidget = SuperWidget.extend({
    //variáveis da widget
    variavelNumerica: null,
    variavelCaracter: null,

    //método iniciado quando a widget é carregada
    init: function() {
        var ds = DatasetFactory.getDataset('getPessoal', null, null, null).values;
        var count = 0;
       $.each(ds, function (indexInArray, valueOfElement) { 
            if(!$('option').hasClass(valueOfElement.nomegestor)){
                $('#nomeGestor').append('<option class="'+valueOfElement.nomegestor+'" value="'+valueOfElement.nomegestor+'">'+valueOfElement.nomegestor+'</option>');
            }
       });

    },
  
    //BIND de eventos
    bindings: {
        local: {
            'buscar': ['click_search']
        },
        global: {}
    },
 
    search: function() {
        
    }

});

