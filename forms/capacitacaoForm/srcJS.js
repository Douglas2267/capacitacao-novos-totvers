$(document).on('change','#dtFinal', function() {
    $('#prazo').val($('#dtFinal').val());
});
function winDown() {
    window.scrollBy(0, 200);
}
$('#dtInicio').on('click', function() {
    winDown();
});

$('#dtFinal').on('click',function(){
    winDown();
});

$(document).on('change', "#nome", function() {
    $('#gestor').val('');
    $('#idgestor').val('');
    $('#nomeGestor').val('');
    $('#area').val('');
    
    var nome = $('#nome').val();
    var gestor = null;
    var f = DatasetFactory.createConstraint("tablename","pessoal","pessoal",ConstraintType.MUST);
    var pessoas = DatasetFactory.getDataset('getPessoal', null, [f], null).values;
    var e ='';
    pessoas.forEach(element => {
        
        if(nome == element.nome){
            var f2 = DatasetFactory.createConstraint("id",element.masterid,element.masterid,ConstraintType.MUST);
            gestor = DatasetFactory.getDataset('getPessoal', null, [f2], null).values;
            e = element.area
        }

        if(gestor != null){
            $('#area').val(e);
            $('#nomeDestino').val($('#nome').val());
            $('#gestor').val(gestor[0].nomegestor);
            $('#idgestor').val(gestor[0].idgestor);
            $('#nomeGestor').val(gestor[0].nomegestor);
        }
    });
});

$(document).on('change', "#nCerti", function () {

    var numeroCertificado = $('#nCerti').val();

    $('#mostraCertificado').val(numeroCertificado);
  
});


function setSelectedZoomItem(selectedItem) { 
    if(selectedItem.inputId == "nome"){
		document.getElementById("idNome").value = selectedItem["colleagueId"];
    }    
}

var clicado=true;
var pasta = $("#campohidden").val();

var guarda = new Array();
var that=this;

$('#acessa').attr('href', 'http://fluighml.totvsrs.com.br:8080/portal/p/1/ecmnavigation?app_ecm_navigation_doc='+pasta);


$('#botaoajax').click(function(){
    
if(clicado){
    $.ajax({
        async: true,
        type: "GET",
        url: '/api/public/ecm/document/listDocument/' + pasta,
        dataType: 'json',
        success: function (response) {

            // var link = JSON.parse(response);

            // for (i=0; i<response.content.length; i++){
            // 	console.log(response.content.fileURL);
                
            // }
            var c = 1
            if(onlyPDFs(response.content)){
                for (var i = 0; i < response.content.length; i++) {
                    console.log(response.content[i].fileURL);
                    console.log(response.content[i].description)
                    var desc = response.content[i].description;
                    if(!desc.toLowerCase().includes('rotei')){
                        $("#appendendo").append($('<a class="btn btn-primary" target="_blank" href="'+response.content[i].fileURL+'">Acessar Certificado&nbsp'+[c]+'</a><br><br>'));
                        c++;
                    }
                }
            }else{
                for (var i = 0; i < response.content.length; i++) {
                    console.log(response.content[i].fileURL);
                    console.log(response.content[i].description)
                    var desc = response.content[i].description;
                    if(!desc.includes('.doc')){
                        $("#appendendo").append($('<a class="btn btn-primary" target="_blank" href="'+response.content[i].fileURL+'">Acessar Certificado&nbsp'+[c]+'</a><br><br>'));
                        c++;
                    }
                }
            }   
    
        }
    });
}
that.clicado= false;
    
});

function onlyPDFs(arr){
    var count = 0
    for(var i = 0; i < arr.length; i++){
        if(arr[i].description.includes('.pdf')){
            count++;
        }
    }

    if(count == arr.length){
        return true;
    }

    return false;
}
