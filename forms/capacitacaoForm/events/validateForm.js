function validateForm(form){

    var numAtiv = getValue("WKNumState");
    var msg = '';
    var nome = form.getValue("nome");
    var inicio = form.getValue("dtInicio");
    var fim = form.getValue("dtFinal");

    var valida = form.getValue("valid");



 	if(numAtiv == 0 || numAtiv == 4){
        if(nome == "" || nome == undefined || nome == null){

         msg += "É necessário preencher o campo nome.<br>"
        }

        if(inicio == "" || inicio == undefined || inicio == null){

          msg += "É necessário selecionar a data de inicio da capacitação.<br>"
        }


        if(fim == "" || fim == undefined || fim == null){

            msg += "É necessário selecionar a data de fim da capacitação.<br>"
        }

        inicio = inicio.split('/');
        fim = fim.split('/');
        var anoIn = parseInt(inicio[inicio.length -1]);
        var mesIn = parseInt(inicio[1]);
        var diaIn = parseInt(inicio[0]);
        var anoFim = parseInt(fim[fim.length -1]);
        var mesFim = parseInt(fim[1]);
        var diaFim = parseInt(fim[0]);
        for(var i = 0; i < inicio.length; i++){
          if(isNaN(inicio[i])){
            msg += "Data Invalida"
          }
        }
        for(var i = 0; i < fim.length; i++){
          if(isNaN(fim[i])){
            msg += "Data Invalida"
          }
        }
        if(anoIn > anoFim){
          msg += 'Data invalida'
        }else if(mesIn > mesFim){
          msg += 'Data invalida'
        }else if(mesIn == mesFim){
          if(diaIn > diaFim){
              msg += 'Data invalida'
          }
        }

        if(msg != '')
        throw msg;

     }

     if(numAtiv == 8 && numAtiv == 9){
        if(valida == "" || valida == undefined || valida == null){

             throw msg += "É necessário confirmar se a capacitação foi completa.<br>"
           }
     }

}